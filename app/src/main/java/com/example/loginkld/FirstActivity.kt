package com.example.loginkld

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_first.*

class FirstActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first)
        init()
        login()

        Glide.with(this)
            .load("https://scontent.ftbs6-1.fna.fbcdn.net/v/t1.15752-9/79083018_992306887808114_1880854062539210752_n.jpg?_nc_cat=110&_nc_ohc=3MspvJAO2zEAQkrFCLBJCXfhRKdovUQdoMaIApDhx0tlzyIOWf8Skvzew&_nc_ht=scontent.ftbs6-1.fna&oh=028340a5e68f9cad4d3f4845490ab0aa&oe=5E72A132")
            .into(imageView)

    }

    private fun init() {
        logInButton.setOnClickListener {
            if (emailEditText.text.isNotEmpty() && passEditText.text.isNotEmpty()) {
                Toast.makeText(this, "Log In is success", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()


            }

        }

    }

    private fun login() {
        logInButton.setOnClickListener {
            val intent = Intent(this, SecondActivity::class.java)
            startActivity(intent)
        }
    }

}
