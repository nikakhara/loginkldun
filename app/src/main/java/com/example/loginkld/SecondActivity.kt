package com.example.loginkld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        Glide.with(this)
            .load("https://scontent.ftbs6-1.fna.fbcdn.net/v/t1.0-9/78454050_2497328720366276_6585259396865982464_n.jpg?_nc_cat=106&_nc_ohc=ZKOMWtDHVm0AQn07YIagDNVceeKSnVS3gg53_E5KkMQc5vmprvdh1GdWA&_nc_ht=scontent.ftbs6-1.fna&oh=5fb6cc0cc6586aafddd1dd2c55f5e14d&oe=5E84A5BB")
            .into(profiileImageView)

        Glide.with(this)
            .load("https://scontent.ftbs6-1.fna.fbcdn.net/v/t1.15752-9/77386694_441415586526749_3815625082928103424_n.jpg?_nc_cat=106&_nc_ohc=pXrTbNVTmgoAQnnULD5efshGA_vfhcIEm2g9Hc050whKglJ0rpFK7yoaw&_nc_ht=scontent.ftbs6-1.fna&oh=24460c50aa98e99401e5b4dd1d20fb9a&oe=5E3E05EC")
            .into(coverImageView)
    }
}

